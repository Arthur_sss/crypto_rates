from config.config import Config, load_config
import requests

config: Config = load_config(".env")
API_KEY = config.key


def get_rate(token, quote):
    url = f"https://rest.coinapi.io/v1/exchangerate/{token}/{quote}/apikey-{API_KEY}"

    response = requests.get(url)

    if response.status_code == 200:
        data = response.json()
        return data["rate"]
    else:
        with open("logs.txt", "a") as file:
            file.write(f"Error: Status code {response.status_code}\n")
            file.write(response.text)
        return "N/A"
