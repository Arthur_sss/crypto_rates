
*crypto_rates.py — the simple tool used to generate a table with crypto coins and it's rates!*

This service based on [CoinAPI](https://github.com/coinapi/coinapi-sdk).

## Installation

Follow these steps to install and set up the project:

1. Clone the repository:

    ```bash
    git clone https://gitlab.com/Arthur_sss/crypto_rates.git

2. Navigate to the project directory:

    ```bash
    cd crypto_rates

3. Install project dependencies using `pip` and the `requirements.txt` file:

    ```bash
    pip install -r requirements.txt

4. Create a `.env` file following the example `.env.example` with adding your personal API key from [CoinAPI](https://www.coinapi.io/get-free-api-key?email=).

## Usage 

Now you can access the service from a shell like this:

    $ python crypto_rates.py

Here is an example result:

![Crypto rates](example.png)

You can customize the currencies and quotes lists by editing [`tokens.py`](lib/tokens.py) and [`quotes.py`](lib/quotes.py).

## Disclaimer 

Service based on the data provided from CoinAPI API,
we cannot guarantee absolute accuracy of the displayed exchange rates.
You should always confirm current rates before making any transactions.
All rates are for information purposes only and are subject to change without prior notice.
Since rates for actual transactions may vary,
we are not offering to enter into any transaction at any rate displayed.
Displayed rates are composite prices and not intended to be used for investment purposes.

## License

This repository is licensed under the MIT License. See the [LICENSE](LICENSE) file for details.