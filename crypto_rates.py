"""
crypto_rates <https://gitlab.com/Arthur_sss/crypto_rates>
A simple tool used to generate a table with crypto coins and it's rates
License: MIT
Contact: Artur Safin <safinartur@cyberservices.com>
"""

from tabulate import tabulate
from get_rate import get_rate
from lib.tokens import tokens
from lib.quotes import quotes


def main() -> None:
    currency_data = []

    for token in tokens:
        rates = {}

        for quote in quotes:
            rates[quote] = get_rate(token, quote)

        row = {"Currency": token}
        row.update({k: v for k, v in rates.items()})
        currency_data.append(row)

    table = tabulate(currency_data, headers="keys", tablefmt="fancy_grid")

    print("Crypto rates:")
    print(table)


if __name__ == "__main__":
    main()
